
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/point_generators_3.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <time.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_data_structure_3< 
  CGAL::Triangulation_vertex_base_3<K>, 
  CGAL::Triangulation_cell_base_3<K>, 
  CGAL::Parallel_tag
> Tds;
   
typedef CGAL::Delaunay_triangulation_3<K, Tds> Triangulation;
typedef Triangulation::Point Point;
        
void load(const std::string& filename, std::vector<Point>& points) {
   std::ifstream in(filename);
   int nb;
   in >> nb;
   points.resize(0);
   points.reserve(nb);
   for(int i=0; i<nb; ++i) {
      Point p;
      in >> p;
      points.push_back(p);
   }
}

CGAL::Bbox_3 bbox(const std::vector<Point>& points) {
   if(points.size() == 0) {
      return CGAL::Bbox_3();
   }
   double xmin = points[0].cartesian(0);
   double ymin = points[0].cartesian(1);
   double zmin = points[0].cartesian(2);
   double xmax = xmin;
   double ymax = ymin;
   double zmax = zmin;
   for(int i=1; i<points.size(); ++i) {
      xmin = std::min(xmin, points[i].cartesian(0));
      ymin = std::min(ymin, points[i].cartesian(1));
      zmin = std::min(zmin, points[i].cartesian(2));
      xmax = std::max(xmax, points[i].cartesian(0));
      ymax = std::max(ymax, points[i].cartesian(1));
      zmax = std::max(zmax, points[i].cartesian(2));      
   }
   return CGAL::Bbox_3(xmin, ymin, zmin, xmax, ymax, zmax);
}
/*
double now() {
   tms now_tms;
   return double(times(&now_tms)) / 100.0;
}
*/


int main(int argc, char** argv) {
   
   if(argc != 2) {
      std::cerr << "Usage: " << argv[0] << " filename.xyz"
	<< std::endl;
      return 0;
   }
   

   
   std::cerr << "Loading..." << std::endl;
   std::vector<Point> V;
   load(argv[1],V);
   std::cerr << "Loaded " << V.size() << " points" << std::endl;

   //double start = now();
   clock_t start = clock();
   std::cerr << "Triangulating..." << std::endl;
   
   // Construct the locking data-structure, using the bounding-box of the points
   Triangulation::Lock_data_structure locking_ds(bbox(V), 50);
   
   //Construct the triangulation in parallel
   Triangulation T(V.begin(), V.end(), &locking_ds);
   
   //double elapsed = (now() - start);
   double elapsed = double(clock() - start);
   
   std::cerr << "Elapsed time: " << elapsed << std::endl;
   std::cerr << "Nb (real) tets: " << T.number_of_finite_cells() << std::endl;
   std::cerr << double(T.number_of_finite_cells()) / elapsed << " Tets per second" << std::endl;
   
//   assert(T.is_valid());
   return 0;
}
